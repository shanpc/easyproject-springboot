package cn.easyutil.easyproject.easySpringboot.mapper;

import java.util.List;

import cn.easyutil.easyproject.easySpringboot.bean.UserBean;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface UserMapper extends BaseMapper<UserBean> {

	List<UserBean> testlistPage(UserBean user);
}
