package cn.easyutil.easyproject.easySpringboot.mapper;

import java.util.List;

import com.example.shyFly.easySql.bean.SQLExecuter;

public interface BaseMapper<T> {

	/**
	 * 分页查询
	 * 
	 * @param t
	 *            依据T中不为null的属性进行查询
	 * @return 返回页码对应的数据集合
	 */
	List<T> listPage(T t);

	List<T> listPage(SQLExecuter executer);

	/**
	 * 修改数据 (id为null,抛出异常)
	 * 
	 * @param t
	 *            依据T中id进行修改
	 * @return 修改条数
	 */
	Integer update(T t);

	Integer update(SQLExecuter executer);

	/**
	 * 新增数据 (无参数返回异常)
	 * 
	 * @param t
	 *            依据不为空的字段进新增
	 * @return 返回条数
	 */
	Integer add(T t);

	Integer add(SQLExecuter executer);

	/**
	 * 依据id查询一条数据
	 * 
	 * @param t
	 *            依据T中不为空的字段作为条件
	 * @return 返回单条数据
	 */
	T get(T t);

	T get(SQLExecuter executer);

	/**
	 * 依据对象属性值删除数据 (无参数返回异常)
	 * 
	 * @param t
	 *            依据T中不为空的字段进行删除
	 * @return
	 */
	Integer delete(T t);

	Integer delete(SQLExecuter executer);

	/**
	 * 依据对象属性查询所有数据 (无分页，全量查询)
	 * 
	 * @param t
	 *            依据T中不为null的属性进行查询
	 * @return 数据集合
	 */
	List<T> select(T t);

	List<T> select(SQLExecuter executer);

	/**
	 * 获取条数
	 */
	Integer count(T query);

	Integer count(SQLExecuter executer);

}
