package cn.easyutil.easyproject.easySpringboot.context;

/**
 * 系统内部公共常量定义
 */
public interface Context {

    /**
     * session中存储用户类型的key
     */
    String session_user = "session_user";

    /** 用户请求接口地址*/
    String threadLocal_request_url = "session_request_url";

    /**
     * session 短信验证码
     */
    String session_sms_code = "session_sms_code";

    /**
     * session 手机号
     */
    String session_sms_phone = "session_sms_phone";

    /**
     * 客户端与服务器交互的秘钥串的长度
     */
    Integer session_token_length = 16;

    /**
     * session中存储token串的key
     */
    String session_token = "session_token";

    /**
     * session中存储userId的key
     */
    String session_userId = "session_userId";
    
    /** 后台用户*/
    String session_back_user = "session_back_user";
    
    /** 后台用户id*/
    String session_back_user_id = "session_back_user_id";
}
