package cn.easyutil.easyproject.easySpringboot.context;

/**
 * 异常code
 */
public enum Error {
	// =================== 系统异常 ============================
	/**
	 * 系统异常
	 */
	system_error(9999, "系统异常", "服务异常,请与客服联系"),

	/**
	 * 缺少请求参数
	 */
	system_lost_param(101, "缺少请求参数", "缺少请求数据,请检查", "系统异常,请与客服联系"),
	/**
	 * 解密token不存在, 请求未加密
	 */
	system_msg_AES_notfind(102, "解密token不存在, 请求未加密", "请求超时，请重试"),
	/**
	 * 解密处理异常, 加密数据不完整
	 */
	system_msg_AES_error(103, "解密处理异常, 加密数据不完整", "请求超时，请重试"),
	/**
	 * 请求加密接口，未建立加密token
	 */
	biz_nottoken(104, "请求加密接口，未建立加密token", "请求超时，请重试"),
	/**
	 * 请求参数验证错误
	 */
	biz_params_error(105, "请求参数验证错误"),
	/**
	 * 请传入分页信息
	 */
	biz_page_params(106, "请传入分页信息", "请传入分页信息"),
	/** 用户未登录*/
	biz_user_not_login(110,"用户未登录"),
	/**
	 * 公共异常
	 */
	biz_error(201, ""),
	/**
	 * 没有选择上传文件
	 */
	file_upload_empty(202, "没有选择上传文件"),
	/**
	 * 未找到对应的文件
	 */
	file_download_notfind(203, "未找到对应的文件"),
	
	


	/**
	 * 未知异常
	 */
	error(-1, "未知异常,未知异常，请与客服联系");
	/**
	 * 编码值
	 */
	private int code;
	/**
	 * 备注，提示
	 */
	private String remark;
	/**
	 * 异常显示问题
	 */
	private String[] viewMsg;

	/**
	 * 异常枚举类
	 *
	 * @param code
	 *            编码值
	 * @param remark
	 *            备注，提示
	 */
	private Error(int code, String remark, String... viewMsg) {
		this.code = code;
		this.remark = remark;
		this.viewMsg = viewMsg;
	}

	/**
	 * 依据异常编码值获取异常
	 */
	public static Error getErrorCode(int code) {
		for (Error ec : Error.values()) {
			if (ec.getCode() == code) {
				return ec;
			}
		}
		return error;
	}

	public int getCode() {
		return code;
	}

	public String getRemark() {
		return remark;
	}

	public String getViewMsg() {
		return viewMsg != null && viewMsg.length > 0 ? viewMsg[0] : remark;
	}
}
