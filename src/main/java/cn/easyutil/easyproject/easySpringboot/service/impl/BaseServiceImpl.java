package cn.easyutil.easyproject.easySpringboot.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.easyJavaUtil.ObjectUtil;
import com.example.shyFly.easySql.EasySqlExecution;
import com.example.shyFly.easySql.bean.EasySqlResult;
import com.example.shyFly.easySql.bean.SQLExecuter;
import cn.easyutil.easyproject.easySpringboot.context.Error;
import cn.easyutil.easyproject.easySpringboot.exception.CommonException;
import cn.easyutil.easyproject.easySpringboot.service.BaseService;

@Service
public class BaseServiceImpl implements BaseService{

	protected EasySqlExecution easySqlExecution = new EasySqlExecution();
	
	@Override
	public <T> T add(T t){
		ObjectUtil.setAttribute(t, "create_time", System.currentTimeMillis());
		easySqlExecution.insert(t);
		return t;
	}
	
	@Override
	public <T> T get(SQLExecuter executer){
		return easySqlExecution.selectOne(executer);
	}
	
	@Override
	public <T> T get(T t){
		SQLExecuter executer = new SQLExecuter(t);
		return get(executer);
	}
	
	@Override
	public <T> List<T> list(SQLExecuter executer){
		return easySqlExecution.select(executer);
	}
	
	@Override
	public <T> List<T> list(T t){
		SQLExecuter executer = new SQLExecuter(t);
		return list(executer);
	}
	
	@Override
	public <T> List<T> listPage(SQLExecuter executer){
		Object bean = executer.getBean();
		Object page = ObjectUtil.getAttributeValue(bean, "page");
		if(page == null){
			throw new CommonException(Error.biz_page_params);
		}
		EasySqlResult<T> result = easySqlExecution.listPage(executer, ObjectUtil.getAttributeValue(page, "currentPage"), ObjectUtil.getAttributeValue(page, "showCount"));
		com.example.shyFly.easySql.bean.Page p = result.getPage();
		List<T> list = result.getResultList();
		if(list!=null && list.size()>0){
			T t = list.get(0);
			ObjectUtil.setAttribute(page, "totalResult", p.getTotalResult());
			ObjectUtil.setAttribute(page, "totalPage", p.getTotalPage());
			ObjectUtil.setAttribute(t, "page", page);
		}
		return list;
	}
	
	@Override
	public <T> List<T> listPage(T t){
		SQLExecuter executer = new SQLExecuter(t);
		return listPage(executer);
	}
	
	@Override
	public Integer update(SQLExecuter executer){
		return easySqlExecution.update(executer);
	}
	
	@Override
	public <T> Integer update(T t){
		SQLExecuter executer = new SQLExecuter(t);
		return update(executer);
	}
	
	@Override
	public Integer delete(SQLExecuter executer){
		return easySqlExecution.delete(executer);
	}
	
	@Override
	public <T> Integer delete(T t){
		SQLExecuter executer = new SQLExecuter(t);
		return delete(executer);
	}
	
	@Override
	public Integer count(SQLExecuter executer){
		executer.setReturnParam("COUNT(1)");
		return queryNumber(executer).intValue();
	}
	
	@Override
	public <T> Integer count(T t){
		SQLExecuter executer = new SQLExecuter(t);
		return count(executer).intValue();
	}
	
	@Override
	public Double max(SQLExecuter executer,String field){
		executer.setReturnParam("MAX("+field+")");
		return queryNumber(executer);
	}
	
	@Override
	public Double min(SQLExecuter executer,String field){
		executer.setReturnParam("MIN("+field+")");
		return queryNumber(executer);
	}
	
	@Override
	public Double sum(SQLExecuter executer,String field){
		executer.setReturnParam("SUM("+field+")");
		return queryNumber(executer);
	}
	
	private Double queryNumber(SQLExecuter executer){
		return easySqlExecution.selectCount(executer);
	}
}
