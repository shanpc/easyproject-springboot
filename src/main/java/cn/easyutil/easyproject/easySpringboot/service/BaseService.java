package cn.easyutil.easyproject.easySpringboot.service;

import java.util.List;

import com.example.shyFly.easySql.bean.SQLExecuter;

public interface BaseService {

	<T> T add(T t);
	
	<T> T get(SQLExecuter executer);
	
	<T> T get(T t);
	
	<T> List<T> list(SQLExecuter executer);
	
	<T> List<T> list(T t);
	
	<T> List<T> listPage(SQLExecuter executer);
	
	<T> List<T> listPage(T t);
	
	Integer update(SQLExecuter executer);
	
	<T> Integer update(T t);
	
	Integer delete(SQLExecuter executer);
	
	<T> Integer delete(T t);
	
	Integer count(SQLExecuter executer);
	
	<T> Integer count(T t);
	
	Double max(SQLExecuter executer,String field);
	
	Double min(SQLExecuter executer,String field);
	
	Double sum(SQLExecuter executer,String field);
}
