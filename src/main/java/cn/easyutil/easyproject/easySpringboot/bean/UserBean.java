package cn.easyutil.easyproject.easySpringboot.bean;

import cn.easyutil.easyproject.easySpringboot.bean.common.BaseBean;
import com.example.shyFly.easySql.annotations.TableName;

@TableName(name="user")
public class UserBean extends BaseBean {

	private String username;
	private String password;
	private Integer age;
	private Double money;
	public String getUsername() {
		return username;
	}
	public String getPassword() {
		return password;
	}
	public Integer getAge() {
		return age;
	}
	public Double getMoney() {
		return money;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public void setMoney(Double money) {
		this.money = money;
	}
	
	
}
