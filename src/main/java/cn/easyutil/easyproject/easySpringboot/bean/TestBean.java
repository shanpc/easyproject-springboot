package cn.easyutil.easyproject.easySpringboot.bean;

import cn.easyutil.easyproject.easySpringboot.bean.common.BaseBean;
import com.yifei.javadoc.util.JsonUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TestBean extends BaseBean {

    private String name;

    private Integer age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public static void main(String[] args) {
        List<TestBean> list = new ArrayList<>();
        TestBean t1 = new TestBean();
        t1.setId(1L);
        t1.setAge(5);
        t1.setName("小明");
        TestBean t2 = new TestBean();
        t2.setId(2L);
        t2.setAge(1);
        t2.setName("小红");
        TestBean t3 = new TestBean();
        t3.setId(3L);
        t3.setAge(10);
        t3.setName("小花");
        TestBean t4 = new TestBean();
        t4.setId(4L);
        t4.setAge(6);
        t4.setName("joy");
        TestBean t5 = new TestBean();
        t5.setId(5L);
        t5.setAge(8);
        t5.setName("jock");
        list.add(t1);
        list.add(t2);
        list.add(t3);
        list.add(t4);
        list.add(t5);
        int sum = list.stream().map(TestBean::getAge).collect(Collectors.toList())
                .stream().sorted((a1, a2) -> a2 - a1).mapToInt(a -> a.intValue()).sum();
        System.out.println(JsonUtil.beanToJson(sum));

    }
}
