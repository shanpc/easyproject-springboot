package cn.easyutil.easyproject.easySpringboot.bean.common;

import java.io.Serializable;

public class ResponseBody implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private int code;
	
	private int errorCode;
	
	private String errorMsg;
	
	private Object data;

	private Page page;
	public int getCode() {
		return code;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public Object getData() {
		return data;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}
	
	
	

}
