package cn.easyutil.easyproject.easySpringboot.bean.common;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("文件类")
public class FileBean implements Serializable {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty("本地访问地址 ")
	private String localPath;
	@ApiModelProperty("oss访问地址 ")
	private String ossPath;
	@ApiModelProperty("文件大小 单位:byte")
	private long size;
	@ApiModelProperty("后缀名")
	private String suffix;
	@ApiModelProperty("文件的md5值")
	private String md5;

	public String getMd5() {
		return md5;
	}

	public void setMd5(String md5) {
		this.md5 = md5;
	}

	public String getLocalPath() {
		return localPath;
	}

	public void setLocalPath(String localPath) {
		this.localPath = localPath;
	}

	public String getOssPath() {
		return ossPath;
	}

	public void setOssPath(String ossPath) {
		this.ossPath = ossPath;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
}
