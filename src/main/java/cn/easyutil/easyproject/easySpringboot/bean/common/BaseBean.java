package cn.easyutil.easyproject.easySpringboot.bean.common;

import java.io.Serializable;

import com.example.shyFly.easySql.annotations.TableId;
import com.example.shyFly.easySql.annotations.TableIgnore;

import io.swagger.annotations.ApiModel;

@ApiModel("")
public class BaseBean implements Serializable{

	private static final long serialVersionUID = -5567744682673510137L;

	@TableId
	private Long id;
	private Long create_time;
	@TableIgnore
	private Integer showCount;
	@TableIgnore
	private Integer currentPage;
	@TableIgnore
	private Page page;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCreate_time() {
		return create_time;
	}
	public void setCreate_time(Long create_time) {
		this.create_time = create_time;
	}
	public Integer getShowCount() {
		return showCount;
	}
	public void setShowCount(Integer showCount) {
		if(this.page == null){
			this.page = new Page();
		}
		this.page.setShowCount(showCount);
	}
	public Integer getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(Integer currentPage) {
		if(this.page == null){
			this.page = new Page();
		}
		this.page.setCurrentPage(currentPage);
	}
	public Page getPage() {
		return page;
	}
	public void setPage(Page page) {
		this.page = page;
	}
	
	
}
