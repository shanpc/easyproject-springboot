package cn.easyutil.easyproject.easySpringboot.bean.common;

import java.lang.reflect.Field;

public class Page extends BaseBean {
    private static final long serialVersionUID = 1L;

    private Integer showCount = 5; // 每页显示记录数

    private Integer totalPage = 0; // 总页数

    private Integer totalResult = 0; // 总记录数

    private Integer currentPage = 1; // 当前页

    private Integer currentResult = 0; // 当前记录起始索引
    private Boolean entityOrField = false; // true:需要分页的地方，传入的参数就是Page实体；false:需要分页的地方，传入的参数所代表的实体拥有Page属性
    private String params = "";

    /**
     * 是否需要分页拼接(true:pageStr和mobileStr的geter返回为拼接值，false：返回值为null)
     */
    private Boolean isNeedPageStr = false;

    public String getParams() {
        return params;
    }

    /**
     * 不需要自动拼接 pageStr和mobileStr 的值
     */
    public void needPageStr() {
        isNeedPageStr = true;
    }

    public void setParams(Object param) throws IllegalAccessException {
        Class clazz = param.getClass();
        Field[] fs = clazz.getDeclaredFields();
        for (Field f : fs) {
            f.setAccessible(true);
            Object value = f.get(param);
            if (value instanceof Page) {
                continue;
            }
            f.setAccessible(false);
            if (value != null && !value.toString().trim().equals("") && !value.toString().trim().equals("0")) {
                params += "&" + f.getName() + "=" + value.toString();
            }
        }
    }


    public Integer getTotalPage() {
        if (showCount == 0 && totalResult == 0) {
            return 0;
        }

        if (totalResult == null) {
            totalResult = 0;
        }

        if (totalResult % showCount == 0) {
            totalPage = totalResult / showCount;
        } else {
            totalPage = totalResult / showCount + 1;
        }
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public Integer getTotalResult() {
        return totalResult;
    }

    public void setTotalResult(Integer totalResult) {
        if (showCount == 0) {
            this.showCount = totalResult;
        }
        this.totalResult = totalResult;
    }

    public Integer getCurrentPage() {
        if (getTotalPage() != 0 && currentPage > getTotalPage()) {
            currentPage = getTotalPage();
        }
        if (currentPage <= 0) {
            currentPage = 1;
        }
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }
    public Integer getShowCount() {
        return showCount;
    }

    public void setShowCount(Integer showCount) {
        this.showCount = showCount;
    }

    public Integer getCurrentResult() {
        currentResult = (getCurrentPage() - 1) * getShowCount();
        if (currentResult < 0) {
            currentResult = 0;
        }
        return currentResult;
    }

    public void setCurrentResult(Integer currentResult) {
        this.currentResult = currentResult;
    }

    public Boolean isEntityOrField() {
        return entityOrField;
    }

    public void setEntityOrField(Boolean entityOrField) {
        this.entityOrField = entityOrField;
    }

}
