package cn.easyutil.easyproject.easySpringboot.controller;


import java.util.List;

import com.yifei.javadoc.util.JsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.easyJavaUtil.StringUtil;
import cn.easyutil.easyproject.easySpringboot.bean.UserBean;
import cn.easyutil.easyproject.easySpringboot.bean.common.Page;
import cn.easyutil.easyproject.easySpringboot.mapper.UserMapper;
import cn.easyutil.easyproject.easySpringboot.service.UserService;
import cn.easyutil.easyproject.easySpringboot.util.RequestPool;
import com.example.shyFly.easySql.bean.SQLExecuter;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags="用户")
@RestController
@RequestMapping("/apis")
public class UserController {
	
	@Autowired
	private UserService userService;
	@Autowired
	private UserMapper userMapper;

	@ApiOperation("添加用户")
	@RequestMapping("/20001")
	public UserBean addProtocol(@RequestBody UserBean bean){
		RequestPool.setSessionAttribute("test", StringUtil.getRandomString(6));
		if(1==1){
			return null;
		}
		return userService.add(null);
	}
	
	@RequestMapping("/20002")
	public List<UserBean> test1(){
		UserBean user = new UserBean();
		SQLExecuter executer = new SQLExecuter(user);
		executer.gte("create_time", 1);
		Page page = new Page();
		page.setShowCount(2);
		page.setCurrentPage(1);
		user.setPage(page);
		user.setCreate_time(2L);
		List<UserBean> list = userMapper.testlistPage(user);
		if(list!=null && !list.isEmpty()){
			list.get(0).setPage(user.getPage());
		}
		return list;
	}
	
	@RequestMapping("/20003")
	public void test2(){
		UserBean user = new UserBean();
		SQLExecuter executer = new SQLExecuter(user).sum("create_time");
		System.out.println("====="+userMapper.get(executer).getCreate_time());
//		System.out.println("======="+userService.sum(executer,"create_time"));
	}

	@RequestMapping("/test")
	public void test(Object obj){
		if(obj == null){
			System.out.println("obj is null");
		}
		System.out.println(obj.getClass());
		System.out.println(JsonUtil.beanToJson(obj));
	}
}
